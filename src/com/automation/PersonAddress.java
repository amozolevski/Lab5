package com.automation;

public class PersonAddress extends Person {
    private String address;

    public PersonAddress(String name, int age, String address) {
        super(name, age);
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean haveName() {
        return name != null ? true : false;
    }
}
