package com.automation;

public class PersonID extends Person{
    private int id;

    public PersonID(String name, int age, int id) {
        super(name, age);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean haveName() {
        return name != null ? true : false;
    }
}
