/*
Интерфейс(Human) -> Абстрактный класс(Person) -> 2 наследника(PersonId & PersonAddress)
 */

package com.automation;

public class Application {
    public static void main(String[] args){
        PersonID personID = new PersonID("qwerty", 25, 880080000);
        PersonID personID2 = new PersonID(null, 50, 770070000);
        PersonAddress personAddress = new PersonAddress("asdfg", 30, "Lenina 29");

        System.out.println(personID.getName() + " " + personID.getAge() + " " + personID.getId() + " " + personID.haveName());
        System.out.println(personID2.getName() + " " + personID2.getAge() + " " + personID2.getId() + " " + personID2.haveName());
        System.out.println(personAddress.getName() + " " + personAddress.getAge() + " " + personAddress.getAddress() + " " + personAddress.haveName());
    }
}
